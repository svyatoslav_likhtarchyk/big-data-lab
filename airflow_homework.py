import pendulum
from airflow import DAG
from airflow.providers.apache.spark.operators.spark_submit import SparkSubmitOperator

default_args = {
    'owner': 'likht',
    'email': ['likhtarchyk.ua@gmail.com']
}

with DAG(
        dag_id='airflow_homework',
        default_args=default_args,
        schedule_interval=None,
        start_date=pendulum.datetime(2022, 6, 9, tz="UTC"),
        tags=['example'],
) as dag:

    pyspark_submit_job = SparkSubmitOperator(
        application="/home/likht/airflow_hw/pyspark_job.py",
        conn_id="spark_default",
        packages="org.apache.spark:spark-avro_2.12:3.2.1",
        task_id="pyspark_job"
    )
