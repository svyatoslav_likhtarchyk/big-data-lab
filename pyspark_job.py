from pyspark.sql import SparkSession


def main():
    spark = SparkSession \
        .builder \
        .appName("arrow-spark") \
        .getOrCreate()

    spark.sparkContext.setLogLevel("ERROR")

    df = spark\
        .read\
        .option('delimiter', '\t')\
        .csv('/home/likht/airflow_hw/airport-codes-na.tsv')

    df_columns = [
        'city',
        'state',
        'country',
        'iata',
    ]

    df = df.toDF(*df_columns)

    df.show(10)

    df.write \
        .format('avro') \
        .save('/home/likht/airflow_output/airport-codes-na.avro')


if __name__ == "__main__":
    main()
