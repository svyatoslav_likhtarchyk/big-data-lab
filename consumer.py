from pyspark.shell import spark

from typing import List, Generator

from pyspark.sql import functions as fn
from pyspark.sql.types import StructType, StringType, TimestampType


CENSURE = '*****'


def main(brokers: str,
         kafka_topic: str,
         path_to_censure_file: str,
         window_duration: int) -> None:


    spark.sparkContext.setLogLevel("ERROR")

    censored_word_list = get_word_list(path_to_censure_file)

    censored_word_list = [ fn.lit(word).alias("censure_word").cast("string") for word in censored_word_list]


    df = spark \
        .readStream \
        .format("kafka") \
        .option("kafka.bootstrap.servers", brokers) \
        .option("subscribe", kafka_topic) \
        .option("startingOffsets", "earliest") \
        .load()

    df = df.selectExpr(
        "CAST(topic AS STRING)",
        "CAST(partition AS STRING)",
        "CAST(offset AS STRING)",
        "CAST(value AS STRING)"
    )

    res_schema = StructType() \
        .add("speaker", StringType()) \
        .add("time", TimestampType()) \
        .add("word", StringType())

    res = df.select(
        fn.col("topic"),
        fn.col("partition"),
        fn.col("offset"),
        fn.from_json(fn.col("value"), res_schema).alias("data")
    )

    res = res.select(
        "data.speaker",
        "data.time",
        "data.word"
    )

    res = res.withColumn("word", fn.when(res.word.isin(censored_word_list), CENSURE).otherwise(res.word))

    df1 = res \
        .withWatermark('time', f'{window_duration} minutes') \
        .groupBy(
        'speaker',
        fn.window('time', f'{window_duration} minutes')) \
        .agg(
        fn.collect_list("word") \
            .alias('word_list'))

    df2 = res \
        .filter(res.word == '*****') \
        .groupBy('speaker') \
        .count()

    df2.writeStream \
        .format('console') \
        .outputMode('complete') \
        .start() \
        .awaitTermination()

def get_word_list(file_path: str) -> Generator:
    with open(file_path, 'r') as f:
        words = [line.strip() for line in f]
        for word in words:
            yield word


if __name__ == '__main__':
    main(brokers='localhost:9092',
         kafka_topic='words',
         path_to_censure_file='censure.txt',
         window_duration=10)
