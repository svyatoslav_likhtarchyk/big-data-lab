-- task 3
CREATE SCHEMA IF NOT EXISTS airport
COMMENT 'Hive airport'
WITH DBPROPERTIES ('data'='2022-04-28','version'='0.0.1');

DESC DATABASE EXTENDED airport;

SELECT current_database();

USE airport;


-- task 4
CREATE TABLE airport_codes (
  City STRING,
  State STRING,
  Country STRING,
  IATA STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t';


-- task 5
DESCRIBE FORMATTED airport_codes;

select * from airport_codes limit 10;


-- task 6
LOAD DATA LOCAL INPATH './airport-codes-na.tsv' OVERWRITE INTO TABLE airport_codes;


-- task 7
select count(*) from airport_codes;


-- task 8
CREATE EXTERNAL TABLE depature_delays_2018Q1(
  fly_date STRING,
  delay INT,
  distance INT,
  origin STRING,
  destination STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ','
STORED as TEXTFILE
LOCATION '/data/airport/airport-codes';


-- task 9
select count(*) from depature_delays_2018Q1;


-- task 10
CREATE TABLE depature_delays (
  fly_date timestamp,
  delay INT,
  distance INT,
  destination STRING
)
PARTITIONED BY(YEAR INT, origin STRING)
CLUSTERED BY(destination) SORTED BY(fly_date) INTO 3 BUCKETS
STORED AS ORC;


-- task 11
SET hive.exec.max.dynamic.partitions.pernode=500;
SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;

INSERT OVERWRITE TABLE depature_delays PARTITION (year=2018, origin)
SELECT
  cast(from_unixtime(unix_timestamp(CONCAT('2018', fly_date), 'yyyyMMddhhmm')) as timestamp) as fly_date,
  delay,
  distance,
  destination,
  origin 
FROM depature_delays_2018Q1;

DESCRIBE FORMATTED depature_delays;

SHOW PARTITIONS depature_delays;

select * from depature_delays limit 10;


-- task 12
SELECT month(fly_date) month, sum(delay) total_delay
FROM depature_delays
GROUP BY month(fly_date);


-- task 13
SELECT t.fly_date, 
t.delay, 
t.orig_code, 
t.orig_country, 
t.orig_city,
t.dest_code,
ac.Country dest_country,
ac.City dest_city 
FROM 
(
SELECT dd.fly_date, 
 dd.delay, 
 dd.origin orig_code, 
 dd.destination dest_code,
 a.Country orig_country, 
 a.City orig_city
 FROM depature_delays dd 
 INNER JOIN airport_codes a 
 ON a.IATA=dd.origin 
 ORDER BY dd.delay DESC 
 LIMIT 10
) t
INNER JOIN airport_codes ac 
ON ac.IATA = t.dest_code
ORDER BY t.delay DESC;


-- task 14
SELECT (
dd.destination,
ac.country, 
ac.city,
avg(dd.delay)
)
FROM depature_delays dd 
INNER JOIN airport_codes ac 
 ON dd.destination = ac.iata 
GROUP BY dd.destination, ac.country, ac.city 
ORDER BY AVG(dd.delay) DESC
LIMIT 10;