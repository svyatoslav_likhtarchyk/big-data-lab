#!/usr/bin/env python3

from itertools import groupby
from operator import itemgetter
from typing import Any
import sys


def mapper_output_gen(file: Any):
    for line in file:
        yield line.rstrip().split('\t', 1)


def main():
    data = mapper_output_gen(sys.stdin)
    for current_word, group in groupby(data, lambda x: x[0]):
        try:
            word_count = sum(map(int, group))
            print(f"{current_word}\t{word_count}")
        except ValueError:
            pass


if __name__ == "__main__":
    main()
