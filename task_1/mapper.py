#!/usr/bin/env python3

import sys
import string
from typing import Any, Generator


def input_gen(file: Any) -> Generator:
    for line in file:
        yield line.split()


def main():
    data = input_gen(sys.stdin)
    for words in data:
        for word in words:
            word = word.translate(str.maketrans("", "", string.punctuation))
            if word.isalpha():
                print(f'{word}\t1')


if __name__ == "__main__":
    main()
