from pprint import pprint
from pymongo import MongoClient

client = MongoClient(f"mongodb+srv://user:Password.2022@likht.yznndrj.mongodb.net/?retryWrites=true&w=majority")

db_rest = client.sample_restaurants
rest = db_rest.restaurants


# 1. Show all cuisines in New York
print('\n\nTask 1\n\n')
new_york_cuisines = rest.distinct('cuisine')
pprint(new_york_cuisines)


# 2. Show all cuisines in Manhattan
print('\n\nTask 2\n\n')
manhattan_cuisines = rest.find(
    {
        'borough': 'Manhattan'
    })\
    .distinct('cuisine')
pprint(manhattan_cuisines)

# 3. Show all pizzerias in Manhattan
print('\n\nTask 3\n\n')
manhattan_pizzerias = rest.find(
    {
        'borough': 'Manhattan',
        'cuisine': {
            '$in': ['Pizza', 'Pizza/Italian']
        }
    })\
    .limit(15)
pprint(list(manhattan_pizzerias))

# 4. Group the data by borough and cuisine, calculate counts and sort by count
print('\n\nTask 4\n\n')
data = rest.aggregate([
    {
        '$group': {
            '_id': {
                'borough': '$borough',
                'cuisine': '$cuisine'
            },
            'count': {
                '$count': {}
            }
        }
    },
    {
        '$sort': {
            'count': -1
        }
    },
    {
        '$limit': 15
    }
])

pprint(list(data))
