import datetime
import random
import time
import json
import logging

from kafka import KafkaProducer, KafkaAdminClient
from kafka.admin import NewTopic

from typing import Dict, List, Generator


def main(brokers: List[str],
         kafka_topic: str,
         speakers: List[str],
         path_to_word_dict: str) -> None:

    admin_client = KafkaAdminClient(bootstrap_servers=brokers,
                                    client_id='test')

    topic_list = [NewTopic(name=kafka_topic,
                           num_partitions=1,
                           replication_factor=1)]

    admin_client.create_topics(topic_list)

    producer = KafkaProducer(bootstrap_servers=brokers,
                             value_serializer=lambda x: json.dumps(x).encode('ascii'))

    current_time = datetime.datetime.now()

    for word in get_word_list(path_to_word_dict):
        current_time += datetime.timedelta(seconds=300)
        message = generate_message(random.choice(speakers), current_time, word)
        time.sleep(.25)

        producer.send(topic=kafka_topic, value=message) \
            .add_callback(lambda x: print(x.topic, x.partition, x.offset, sep='\n')) \
            .add_errback(lambda x: logging.error('I am an errback', exc_info=x))


def get_word_list(file_path: str) -> Generator:
    with open(file_path, 'r') as f:
        words = [line.strip() for line in f]
        random.shuffle(words)
        for word in words:
            yield word


def generate_message(speaker: str, time_: datetime, word: str) -> Dict[str, str]:
    return {
        "speaker": speaker,
        "time": time_.strftime("%Y-%m-%d %H:%M:%S.%f")[:-5],
        "word": word
    }


if __name__ == '__main__':
    main(brokers=['localhost:9092'],
         kafka_topic='words',
         speakers=['Speaker1', 'Speaker2', 'Speaker3'],
         path_to_word_dict='dictionary.txt.txt')
